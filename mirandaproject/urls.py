from django.conf import settings
from django.conf.urls import patterns, include, url, static

from django.contrib import admin
from django.contrib.auth.views import login
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import TemplateView

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^login/$', login,
                           {"template_name": "login.html"}, name='login'),
                       url(r'^$', ensure_csrf_cookie(TemplateView.as_view(template_name="index.html")), name='index'),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^api/', include('website.urls'))
)

if settings.DEBUG:
    urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)