RSC.module("ChatApp", {
    startWithParent: true,
    define: function (ChatApp, App, Backbone, Marionette, $, _) {
        "use strict";

        // Controller
        // ----------

        ChatApp.Controller = Marionette.Controller.extend({
            initialize: function (opts) {
                this.region = opts.region;
                this.region2 = opts.region2;
                this.repo = opts.repo;
                App.vent.on("app:chat:set-online-list", function (listIds) {
                    this.repo.setOnlineUsers(listIds);
                }, this);
                App.vent.on("app:chat:new-user-online", function (userId) {
                    this.repo.newUserConected(userId);
                }, this);
                App.vent.on("app:chat:leave-user-online", function (userId) {
                    this.repo.removeUserConected(userId);
                }, this);
                App.vent.on("app:chat:open-chat", function (user) {
                    this.repo.addContactChatOpen(user);
                }, this);
                App.vent.on("app:chat:close-chat", function (user) {
                    this.repo.removeContactChatOpen(user);
                }, this);
            },
            show: function () {
                var that = this;
                this.repo.getAllContacts().done(function (data) {
                    var view = new ChatApp.ChatView({collection: data});
                    that.region.show(view);
                });
                this.repo.getContactsChatOpen().done(function (data) {
                    var view2 = new ChatApp.ChatWindowsOpen({collection: data});
                    that.region2.show(view2);
                });
            }
        });


        ChatApp.addInitializer(function () {
            ChatApp.controller = new ChatApp.Controller({repo: ChatApp.repository, region: RSC.chat, region2: RSC.chatzone});
            ChatApp.controller.show();
        });

        ChatApp.addFinalizer(function () {
            ChatApp.controller.close();
        });

    }
});