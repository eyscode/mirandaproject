RSC.module("ChatApp", function (ChatApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // ChatApp Models
    // --------------

    ChatApp.ContactModel = Backbone.Model.extend();
    ChatApp.ContactsCollection = Backbone.Collection.extend({
        url: "/api/me/contacts/",
        model: ChatApp.ContactModel
    });
    ChatApp.MsgModel = Backbone.Model.extend();
    ChatApp.MsgsCollection = Backbone.Collection.extend({
        model: ChatApp.MsgModel
    });

    var ChatRepository = Marionette.Controller.extend({
        initialize: function () {
            this.contacts = new ChatApp.ContactsCollection();
            this.contactsChatOpen = new ChatApp.ContactsCollection();
            this.ajaxCall = this.contacts.fetch();
        },
        getContactsChatOpen: function () {
            var defferred = $.Deferred();
            defferred.resolve(this.contactsChatOpen);
            return defferred;
        },
        addContactChatOpen: function (user) {
            this.contactsChatOpen.add(user, {at: 0});
        },
        removeContactChatOpen: function (user) {
            this.contactsChatOpen.remove(user);
        },
        getAllContacts: function () {
            var defferred = $.Deferred();
            var that = this;
            this.ajaxCall.done(function () {
                defferred.resolve(that.contacts);
            });
            return defferred;
        },
        setOnlineUsers: function (listIds) {
            var that = this;
            this.getAllContacts().done(function () {
                for (var i = 0; i < listIds.length; i++) {
                    var user = that.contacts.get(listIds[i].id);
                    if (user) {
                        user.set("online", true);
                    }
                }
            });
        },
        newUserConected: function (userId) {
            var user = this.contacts.get(userId.id);
            if (user) {
                user.set("online", true);
            }

        },
        removeUserConected: function (userId) {
            var user = this.contacts.get(userId);
            if (user) {
                user.set("online", false);
            }
        },
        getLastMsgs: function (userId) {
            var defferred = $.Deferred();
            var contact = this.contacts.get(userId);
            if (!contact.msgs) {
                contact.msgs = new ChatApp.MsgsCollection();
                contact.msgs.url = "/api/me/last-msgs/" + userId + "/";
                contact.ajaxCall = contact.msgs.fetch();
                contact.ajaxCall.done(function () {
                    defferred.resolve(contact.msgs);
                });
            } else {
                contact.ajaxCall.done(function () {
                    defferred.resolve(contact.msgs);
                });
            }
            return defferred;
        },
        newMsgFromServer: function (msg) {
            var contact = this.contacts.get(msg.from_user);
            var that = this;
            if (!contact) {
                App.ProfileApp.repository.getInfo().done(function (me) {
                    if (me.get("id") == msg.from_user) {
                        contact = that.contacts.get(msg.to_user);
                        contact.msgs.add(msg);
                    }
                });
            } else {
                contact.msgs.add(msg);
            }
        }
    });

    App.addInitializer(function () {
        ChatApp.repository = new ChatRepository();
    });

});