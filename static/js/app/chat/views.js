RSC.module("ChatApp", function (ChatApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Nav App Views
    // --------------

    ChatApp.ContactView = Marionette.ItemView.extend({
        template: "#contact-template",
        className: "list-group-item",
        tagName: "a",
        modelEvents: {
            change: "render"
        },
        events: {
            "click": "openChat"
        },
        openChat: function () {
            App.vent.trigger("app:chat:open-chat", this.model);
        }
    });

    ChatApp.ChatView = Marionette.CollectionView.extend({
        className: "list-group",
        itemView: ChatApp.ContactView
    });

    ChatApp.ChatWindowHeader = Marionette.ItemView.extend({
        template: "#chat-window-header-template",
        events: {
            "click .chat-window-close": "closeChat",
            "click": "minimize"
        },
        closeChat: function () {
            console.log("cierrate");
            App.vent.trigger("app:chat:close-window-" + this.model.get("id"));
        },
        minimize: function (e) {
            if (!$(e.target).is("a")) {
                App.vent.trigger("app:chat:minimize-window", this.model.get("id"));
            }
        },
        modelEvents: {
            change: "render"
        }
    });

    ChatApp.ChatMsgView = Marionette.ItemView.extend({
        className: "msg-item",
        template: "#chat-msg-template",
        templateHelpers: {
            me_id: function () {
                return App.ProfileApp.repository.me.get("id");
            }
        }
    });

    ChatApp.ChatViewBody = Marionette.CollectionView.extend({
        className: "list-msgs",
        itemView: ChatApp.ChatMsgView,
        onAfterItemAdded: function () {
            this.$el.scrollTop(this.$el[0].scrollHeight);
        },
        onShow: function () {
            this.$el.scrollTop(this.$el[0].scrollHeight);
        }
    });

    ChatApp.ChatWindowView = Marionette.Layout.extend({
        template: "#chat-window-template",
        className: "chat-window",
        initialize: function () {
            App.vent.on("app:chat:minimize-window", this.minimize, this);
            App.vent.on("app:chat:close-window-" + this.model.get("id"), this.closeChat, this);
        },
        regions: {
            header: ".chat-window-header",
            body: ".chat-window-body",
            footer: ".chat-window-footer"
        },
        events: {
            "keypress textarea": "submitMessage",
            "click .chat-window-body": "focusFooter"
        },
        ui: {
            body: ".chat-window-body",
            footer: ".chat-window-footer",
            textarea: "textarea"
        },
        focusFooter: function () {
            this.ui.textarea.focus();
            return false;
        },
        submitMessage: function (e) {
            if (e.which == 13 && !e.shiftKey) {
                e.preventDefault();
                var that = this;
                if (this.ui.textarea.val().trim() != "") {
                    var text = this.ui.textarea.val();
                    $.ajax({
                        type: "post",
                        url: "/api/me/send-msg/",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({"text": text, "user": this.model.get("id")})
                    });

                    this.ui.textarea.val("");
                    this.ui.textarea.trigger("keyup");
                    this.ui.textarea.trigger("autosize.resize");
                }
            }
        },
        closeChat: function () {
            App.vent.trigger("app:chat:close-chat", this.model);
            this.remove();
            return false;
        },
        minimize: function (userId) {
            if (userId == this.model.get("id")) {
                if (this.ui.body.css("display") == "none") {
                    this.ui.body.show();
                    this.ui.footer.show();
                    this.$el.removeClass("minimized");
                    this.ui.textarea.focus();
                } else {
                    this.$el.addClass("minimized");
                    this.ui.body.hide();
                    this.ui.footer.hide();
                }
            }
        },
        onRender: function () {
            this.ui.textarea.autosize();
        },
        onShow: function () {
            var that = this;
            this.ui.textarea.focus();
            var view = new ChatApp.ChatWindowHeader({model: this.model});
            this.header.show(view);
            ChatApp.repository.getLastMsgs(this.model.get("id")).done(function (msgs) {
                var view_msgs = new ChatApp.ChatViewBody({collection: msgs});
                that.body.show(view_msgs);
            });
        },
        onClose: function () {
            App.vent.off("app:chat:minimize-window", this.minimize);
            App.vent.off("app:chat:close-window" + this.model.get("id"), this.closeChat);
        }
    });

    ChatApp.ChatWindowsOpen = Marionette.CollectionView.extend({
        className: "chat-window-list",
        itemView: ChatApp.ChatWindowView,
        appendHtml: function (collectionView, itemView, index) {
            var childrenContainer = collectionView.itemViewContainer ?
                collectionView.$(collectionView.itemViewContainer) : collectionView.$el;
            var children = childrenContainer.children();
            if (children.size() - 1 < index) {
                childrenContainer.append(itemView.el);
            } else {
                childrenContainer.children().eq(index).before(itemView.el);
            }
        }
    });
});