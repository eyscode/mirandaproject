RSC.module("NavApp", function (NavApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Nav App Views
    // --------------

    NavApp.NavbarView = Marionette.ItemView.extend({
        template: "#navbar-template",
        className: "container",
        modelEvents: {
            "change": "render"
        }
    });

    NavApp.RighNavView = Marionette.ItemView.extend({
        template: "#right-nav-template",
        className: "col-fixed",
        modelEvents: {
            "change": "render"
        }
    });

});