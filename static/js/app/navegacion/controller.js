RSC.module("NavApp", function (NavApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Controller
    // ----------

    NavApp.Controller = Marionette.Controller.extend({
        initialize: function (opts) {
            this.region = opts.region;
            this.region2 = opts.region2;
        },
        show: function () {
            var that = this;
            App.ProfileApp.repository.getInfo().done(function (data) {
                var view = new NavApp.NavbarView({model: data});
                that.region.show(view);
                var view2 = new NavApp.RighNavView({model: data});
                that.region2.show(view2);
            });
        }
    });


    // Initializer
    // -----------

    App.addInitializer(function (args) {
        NavApp.controller = new NavApp.Controller({region: RSC.navbar, region2: RSC.rightnav});
        NavApp.controller.show();
    });

    NavApp.addFinalizer(function () {
        if (NavApp.controller) {
            NavApp.controller.close();
            delete NavApp.controller;
        }
    });
});