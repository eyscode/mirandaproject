RSC.module("ProfileApp", function (ProfileApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // ProfileApp Models
    // --------------

    ProfileApp.ContactModel = Backbone.Model.extend({
        urlRoot: "/api/me/"
    });
    ProfileApp.WorkModel = Backbone.Model.extend({
        defaults: {
            employer: "",
            title: "",
            description: "",
            start_year: "",
            end_year: ""
        }
    });
    ProfileApp.EducationModel = Backbone.Model.extend({
        defaults: {
            school: "",
            degree: "",
            description: "",
            start_year: "",
            end_year: ""
        }
    });


    // ProfileApp Collection

    ProfileApp.WorkCollection = Backbone.Collection.extend({
        url: "/api/me/work/"
    });

    ProfileApp.EducationCollection = Backbone.Collection.extend({
        url: "/api/me/education/"
    });

    ProfileApp.CircleModel = Backbone.Model.extend();

    ProfileApp.CircleCollection = Backbone.Collection.extend({
        url: "/api/me/circles/"
    });

    var InfoRespository = Marionette.Controller.extend({
        initialize: function () {
            this.me = new ProfileApp.ContactModel({urlRoot: "/api/me/"});
            this.circles = new ProfileApp.CircleCollection();
            var that = this;
            this.ajaxCall = this.me.fetch().done(function () {
                var socket = io.connect('http://localhost:3000');
                App.vent.on("app:socket:emit", function (opt) {
                    socket.emit(opt.event, opt.data);
                });

                socket.on('new-user-online', function (data) {
                    App.vent.trigger("app:chat:new-user-online", data);
                });

                socket.on("leave-user-online", function (data) {
                    App.vent.trigger("app:chat:leave-user-online", data);
                });

                socket.on("user-online-list", function (list) {
                    App.vent.trigger("app:chat:set-online-list", list);
                });

                socket.on("chat", function (msg) {
                    App.ChatApp.repository.newMsgFromServer(msg);
                });

                App.vent.trigger("app:socket:emit", {event: "user-conected", data: that.me});
            });
            this.ajaxCall = this.circles.fetch();
        },
        getInfo: function () {
            var defferred = $.Deferred();
            var that = this;
            this.ajaxCall.done(function () {
                defferred.resolve(that.me);
            });
            return defferred;
        },
        getInfoUser: function (userUnique) {
            var defferred = $.Deferred();
            var user = new ProfileApp.ContactModel();
            user.url = "/api/users/" + userUnique + "/";
            user.fetch().done(function () {
                defferred.resolve(user);
            });
            return defferred;
        },
        getWorks: function () {
            var c = new ProfileApp.WorkCollection();
            var defferred = $.Deferred();
            c.fetch({success: function (all) {
                defferred.resolve(all);
            }});
            return defferred;
        },
        getWorksUser: function (userUnique) {
            var c = new ProfileApp.WorkCollection();
            c.url = "/api/users/" + userUnique + "/works/";
            var defferred = $.Deferred();
            c.fetch({success: function (all) {
                defferred.resolve(all);
            }});
            return defferred;
        },
        getEducations: function () {
            var c = new ProfileApp.EducationCollection();
            var defferred = $.Deferred();
            c.fetch({success: function (all) {
                defferred.resolve(all);
            }});
            return defferred;
        },
        getEducationsUser: function (userUnique) {
            var c = new ProfileApp.EducationCollection();
            c.url = "/api/users/" + userUnique + "/educations/";
            var defferred = $.Deferred();
            c.fetch({success: function (all) {
                defferred.resolve(all);
            }});
            return defferred;
        },
        getCircles: function () {
            var defferred = $.Deferred();
            var that = this;
            this.ajaxCall.done(function () {
                defferred.resolve(that.circles);
            });
            return defferred;
        }
    });

    App.addInitializer(function () {
        ProfileApp.repository = new InfoRespository();
    });

});