RSC.module("ProfileApp", function (ProfileApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Nav App Views
    // --------------

    ProfileApp.Layout = Marionette.Layout.extend({
        template: "#profile-layout-template",
        className: "profile-layout",
        regions: {
            personal: ".personal-region",
            works: ".work-region",
            educations: ".education-region"
        }
    });
    ProfileApp.PersonalView = Marionette.ItemView.extend({
        template: "#personal-template",
        events: {
            "click .save-item": "saveView"
        },
        modelEvents: {
            "change": "render"
        },
        ui: {
            first_name: ".first_name",
            last_name: ".last_name",
            email: ".email"
        },
        saveView: function () {
            this.model.save({first_name: this.ui.first_name.val(), last_name: this.ui.last_name.val(),
                email: this.ui.email.val()});
        }
    });
    ProfileApp.WorkView = Marionette.ItemView.extend({
        template: "#work-template",
        className: "work-item",
        events: {
            "click .remove-item": "deleteView",
            "click .save-item": "saveView"
        },
        ui: {
            employer: ".employer",
            title: ".title",
            description: ".description",
            start_year: ".start-year",
            end_year: ".end-year"
        },
        deleteView: function () {
            this.model.destroy();
        },
        saveView: function () {
            this.model.save({employer: this.ui.employer.val(), title: this.ui.title.val(),
                description: this.ui.description.val(), start_year: this.ui.start_year.val(),
                end_year: this.ui.end_year.val()});
        }
    });
    ProfileApp.WorkViewReadOnly = Marionette.ItemView.extend({
        template: "#work-readonly-template",
        className: "work-item"
    });
    ProfileApp.EducationViewReadOnly = Marionette.ItemView.extend({
        template: "#education-readonly-template",
        className: "education-item"
    });
    ProfileApp.EducationView = Marionette.ItemView.extend({
        template: "#education-template",
        className: "education-item",
        events: {
            "click .remove-item": "deleteView",
            "click .save-item": "saveView"
        },
        ui: {
            school: ".school",
            degree: ".degree",
            description: ".description",
            start_year: ".start-year",
            end_year: ".end-year"
        },
        deleteView: function () {
            this.model.destroy();
        },
        saveView: function () {
            this.model.save({school: this.ui.school.val(), degree: this.ui.degree.val(),
                description: this.ui.description.val(), start_year: this.ui.start_year.val(),
                end_year: this.ui.end_year.val()});
        }
    });
    ProfileApp.WorkListView = Marionette.CompositeView.extend({
        itemView: ProfileApp.WorkView,
        template: "#work-list-template",
        itemViewContainer: ".work-list",
        appendHtml: function (collectionView, itemView, index) {
            var childrenContainer = collectionView.itemViewContainer ?
                collectionView.$(collectionView.itemViewContainer) : collectionView.$el;
            var children = childrenContainer.children();
            if (children.size() - 1 < index) {
                childrenContainer.append(itemView.el);
            } else {
                childrenContainer.children().eq(index).before(itemView.el);
            }
        },
        events: {
            "click .add-new": "newWorkBlank"
        },
        newWorkBlank: function () {
            var work_blank = new ProfileApp.WorkModel();
            this.collection.add(work_blank, {at: 0});
        }
    });
    ProfileApp.EducationListView = Marionette.CompositeView.extend({
        itemView: ProfileApp.EducationView,
        template: "#education-list-template",
        itemViewContainer: ".education-list",
        appendHtml: function (collectionView, itemView, index) {
            var childrenContainer = collectionView.itemViewContainer ?
                collectionView.$(collectionView.itemViewContainer) : collectionView.$el;
            var children = childrenContainer.children();
            if (children.size() - 1 < index) {
                childrenContainer.append(itemView.el);
            } else {
                childrenContainer.children().eq(index).before(itemView.el);
            }
        },
        events: {
            "click .add-new": "newEduBlank"
        },
        newEduBlank: function () {
            var edu_blank = new ProfileApp.EducationModel();
            this.collection.add(edu_blank, {at: 0});
        }
    });
});