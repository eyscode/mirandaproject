RSC.module("ProfileApp", {
    startWithParent: false,
    define: function (ProfileApp, App, Backbone, Marionette, $, _) {

        // Router
        // -----------

        var Router = Marionette.AppRouter.extend({
            routes: {
                "profile": "showStream",
                ":username": "showProfile"
            },
            before: function () {
                App.startSubApp("ProfileApp");
            },
            showStream: function () {
                ProfileApp.controller.show();
            },
            showProfile: function (username) {
                App.ProfileApp.repository.getInfo().done(function (me) {
                    if (username == me.get("email").substr(0, me.get("email").indexOf("@"))) {
                        ProfileApp.controller.show();
                    } else {
                        ProfileApp.controller.showUser(username);
                    }
                });
            }
        });

        // Initializer
        // -----------

        App.addInitializer(function () {
            var router = new Router();
        });

    }
});