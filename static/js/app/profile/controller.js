RSC.module("ProfileApp", {
    startWithParent: false,
    define: function (ProfileApp, App, Backbone, Marionette, $, _) {
        "use strict";

        // Controller
        // ----------

        ProfileApp.Controller = Marionette.Controller.extend({
            initialize: function (opts) {
                this.region = opts.region;
                this.repo = opts.repo;
            },
            show: function () {
                var layout = new ProfileApp.Layout();
                this.region.show(layout);
                $.when(this.repo.getInfo()).then(function (me) {
                    var view = new ProfileApp.PersonalView({model: me});
                    layout.personal.show(view);
                });
                $.when(this.repo.getWorks()).then(function (works) {
                    var view = new ProfileApp.WorkListView({collection: works});
                    layout.works.show(view);
                });
                $.when(this.repo.getEducations()).then(function (edus) {
                    var view = new ProfileApp.EducationListView({collection: edus});
                    layout.educations.show(view);
                });
            },
            showUser: function (userUnique) {
                var layout = new ProfileApp.Layout();
                this.region.show(layout);
                $.when(this.repo.getInfoUser(userUnique)).then(function (me) {
                    var view = new ProfileApp.PersonalView({template: "#user-personal-template", model: me});
                    layout.personal.show(view);
                });
                $.when(this.repo.getWorksUser(userUnique)).then(function (works) {
                    if (works.length > 0) {
                        var view = new ProfileApp.WorkListView({template: "#user-work-list-template", collection: works, itemView: ProfileApp.WorkViewReadOnly});
                        layout.works.show(view);
                    }
                });
                $.when(this.repo.getEducationsUser(userUnique)).then(function (edus) {
                    if (edus.length > 0) {
                        var view = new ProfileApp.EducationListView({template: "#user-education-list-template", collection: edus, itemView: ProfileApp.EducationViewReadOnly});
                        layout.educations.show(view);
                    }
                });
            }
        });


        ProfileApp.addInitializer(function () {
            ProfileApp.controller = new ProfileApp.Controller({repo: ProfileApp.repository, region: RSC.content});
        });

        ProfileApp.addFinalizer(function () {
            ProfileApp.controller.close();
        });

    }
});