RSC.module("StreamApp", {
    startWithParent: false,
    define: function (StreamApp, App, Backbone, Marionette, $, _) {

        // Router
        // -----------

        var Router = Marionette.AppRouter.extend({
            routes: {
                "": "showStream",
                "home": "showStream"
            },
            before: function () {
                App.startSubApp("StreamApp");
            },
            showStream: function () {
                StreamApp.controller.show();
            }
        });

        // Initializer
        // -----------

        App.addInitializer(function () {
            var router = new Router();
        });

    }
});