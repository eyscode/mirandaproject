RSC.module("StreamApp", function (StreamApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Nav App Views
    // --------------

    StreamApp.Layout = Marionette.Layout.extend({
        template: "#stream-layout-template",
        regions: {
            newPost: "#new-post-region",
            listPost: "#post-stream-region"
        }
    });

    StreamApp.NewPostView = Marionette.ItemView.extend({
        template: "#new-post-template",
        className: "post-box",
        events: {
            "focus textarea": "onFocus",
            "click #publicate_pub": "onClick"
        },
        onFocus: function () {
            this.$(".footer").show();
        },
        ui: {
            textarea: "textarea",
            select: "select"
        },
        onClick: function () {
            var data = {text: this.ui.textarea.val()};
            data.values = _.map(this.ui.select.find("option:selected"), function (opt) {
                return parseInt(opt.value);
            });
            if (data.text) {
                App.vent.trigger("app:stream:create-post", data);
                this.ui.textarea.val("");
                this.ui.textarea.trigger("keyup");
                this.ui.textarea.trigger("autosize.resize");
                this.ui.select.val('').trigger("chosen:updated");
            }
        },
        onShow: function () {
            this.$("select").chosen({width: "82%", height: "40px"});
            var options = [
                {label: "George Washington", value: "george"},
                {label: "Abraham Lincoln", value: "abe"},
                {label: "Andrew Jackson", value: "andy"},
                {label: "Thomas Jefferson", value: "tj"},
                {label: "Alexander Hamilton", value: "alex"},
                {label: "John F. Kennedy", value: "jfk"},
                {label: "Teddy Roosevelt", value: "teddy"},
                {label: "Franklin Delano Roosevelt", value: "fdr"}
            ];

            this.ui.textarea.tagmate({
                exprs: {
                    "@": Tagmate.NAME_TAG_EXPR,
                    "#": Tagmate.HASH_TAG_EXPR,
                    "$": Tagmate.PRICE_TAG_EXPR,
                    "£": Tagmate.PRICE_TAG_EXPR
                },
                sources: {
                    "@": function (request, response) {
                        // use convenience filter function
                        var filtered = Tagmate.filterOptions(options, request.term);
                        response(filtered);
                    }
                },
                capture_tag: function (tag) {
                    console.log("Got tag: " + tag);
                },
                replace_tag: function (tag, value) {
                    console.log("Replaced tag: " + tag + " with: " + value);
                },
                highlight_tags: true,
                highlight_class: "hilite",
                menu_class: "menu",
                menu_option_class: "option",
                menu_option_active_class: "active"
            });
            this.$("#new_post").autosize();
            this.$(".menu").css("width", this.$(".textarea-container").width());
        }
    });

    StreamApp.CommentView = Marionette.ItemView.extend({
        template: "#comment-template",
        className: "comment-item",
        onRender: function () {
            this.$(".date").timeago();
        },
        events: {
            "click .make-like-comment": "makeLike"
        },
        makeLike: function () {
            var that = this;
            $.ajax({
                type: "POST", url: "/api/comments/" + this.model.get("id") + "/toggle-like/"
            }).done(function (data) {
                    if (data.like) {
                        that.model.set({"likes": that.model.get("likes") + 1, melike: true});
                    } else {
                        that.model.set({"likes": that.model.get("likes") - 1, melike: false});
                    }
                });
        },
        modelEvents: {
            "change": "render"
        }
    });

    StreamApp.PostView = Marionette.CompositeView.extend({
        template: "#post-template",
        className: "post-item",
        itemView: StreamApp.CommentView,
        itemViewContainer: ".comment-list",
        templateHelpers: {
            me_photo: function () {
                return App.ProfileApp.repository.me.get("profile").photo;
            }
        },
        onRender: function () {
            this.$(".date").timeago();
        },
        initialize: function () {
            var that = this;
            StreamApp.repository.getCommentsFromPost(this.model.get("id")).done(function (data) {
                that.collection = data;
                that._initialEvents();
                that.collection.trigger("reset");
            });
        },
        appendHtml: function (collectionView, itemView, index) {
            var childrenContainer = collectionView.itemViewContainer ?
                collectionView.$(collectionView.itemViewContainer) : collectionView.$el;
            var children = childrenContainer.children();
            if (children.size() - 1 < index) {
                childrenContainer.append(itemView.el);
            } else {
                childrenContainer.children().eq(index).before(itemView.el);
            }
        },
        onShow: function () {
            var options = [
                {label: "George Washington", value: "george"},
                {label: "Abraham Lincoln", value: "abe"},
                {label: "Andrew Jackson", value: "andy"},
                {label: "Thomas Jefferson", value: "tj"},
                {label: "Alexander Hamilton", value: "alex"},
                {label: "John F. Kennedy", value: "jfk"},
                {label: "Teddy Roosevelt", value: "teddy"},
                {label: "Franklin Delano Roosevelt", value: "fdr"}
            ];

            this.$("textarea").tagmate({
                exprs: {
                    "@": Tagmate.NAME_TAG_EXPR,
                    "#": Tagmate.HASH_TAG_EXPR,
                    "$": Tagmate.PRICE_TAG_EXPR,
                    "£": Tagmate.PRICE_TAG_EXPR
                },
                sources: {
                    "@": function (request, response) {
                        // use convenience filter function
                        var filtered = Tagmate.filterOptions(options, request.term);
                        response(filtered);
                    }
                },
                capture_tag: function (tag) {
                    console.log("Got tag: " + tag);
                },
                replace_tag: function (tag, value) {
                    console.log("Replaced tag: " + tag + " with: " + value);
                },
                highlight_tags: true,
                highlight_class: "hilite",
                menu_class: "menu",
                menu_option_class: "option",
                menu_option_active_class: "active"
            });
            this.$("textarea").autosize();
        },
        events: {
            "click .make-like-post": "makeLike",
            "keypress textarea": "submitComment"
        },
        modelEvents: {
            "change": "render"
        },
        ui: {
            textarea: "textarea"
        },
        submitComment: function (e) {
            if (e.which == 13 && !e.shiftKey) {
                e.preventDefault();
                if (this.ui.textarea.val().trim() != "") {
                    var comment = new StreamApp.CommentModel({text: this.ui.textarea.val()});
                    this.ui.textarea.val("");
                    this.ui.textarea.trigger("keyup");
                    this.ui.textarea.trigger("autosize.resize");
                    comment.url = "/api/me/post/" + this.model.get("id") + "/comments/";
                    var that = this;
                    comment.save().done(function (data) {
                        that.collection.add(data);
                    });
                }
            }
        },
        makeLike: function () {
            var that = this;
            $.ajax({
                type: "POST", url: "/api/posts/" + this.model.get("id") + "/toggle-like/"
            }).done(function (data) {
                    if (data.like) {
                        that.model.set({"likes": that.model.get("likes") + 1, melike: true});
                    } else {
                        that.model.set({"likes": that.model.get("likes") - 1, melike: false});
                    }
                });
        }
    });

    StreamApp.PostStreamView = Marionette.CollectionView.extend({
        itemView: StreamApp.PostView,
        initialize: function () {
            App.vent.on("app:stream:create-post", this.addPost, this);
        },
        appendHtml: function (collectionView, itemView, index) {
            var childrenContainer = collectionView.itemViewContainer ?
                collectionView.$(collectionView.itemViewContainer) : collectionView.$el;
            var children = childrenContainer.children();
            if (children.size() - 1 < index) {
                childrenContainer.append(itemView.el);
            } else {
                childrenContainer.children().eq(index).before(itemView.el);
            }
        },
        addPost: function (data) {
            var that = this;
            var post = new StreamApp.PostModel(data);
            post.save().done(function () {
                that.collection.add(post, {at: 0});
            });
        },
        onClose: function () {
            App.vent.off("app:stream:create-post", this.addPost);
        }
    });
});