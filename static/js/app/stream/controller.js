RSC.module("StreamApp", {
    startWithParent: false,
    define: function (StreamApp, App, Backbone, Marionette, $, _) {
        "use strict";

        // Controller
        // ----------

        StreamApp.Controller = Marionette.Controller.extend({
            initialize: function (opts) {
                this.region = opts.region;
                this.repo = opts.repo;
            },
            show: function () {
                var layout = new StreamApp.Layout();
                this.region.show(layout);
                App.ProfileApp.repository.getCircles().done(function (circles) {
                    var newpost = new StreamApp.NewPostView({collection: circles});
                    layout.newPost.show(newpost);
                });
                this.repo.getAll().done(function (all) {
                    var view = new StreamApp.PostStreamView({collection: all});
                    layout.listPost.show(view);
                });
                Backbone.history.navigate("#home")
            }
        });


        StreamApp.addInitializer(function () {
            StreamApp.controller = new StreamApp.Controller({repo: StreamApp.repository, region: RSC.content});
        });

        StreamApp.addFinalizer(function () {
            StreamApp.controller.close();
        });

    }
});