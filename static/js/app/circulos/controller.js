RSC.module("CirculosApp", {
    startWithParent: false,
    define: function (CirculosApp, App, Backbone, Marionette, $, _) {
        "use strict";

        // Controller
        // ----------

        CirculosApp.Controller = Marionette.Controller.extend({
            initialize: function (opts) {
                this.region = opts.region;
                this.repo = opts.repo;
            },
            show: function () {
                var layout = new CirculosApp.Layout();
                this.region.show(layout);
                this.repo.getCircles().done(function (circles) {
                    var view = new CirculosApp.CircleListView({collection: circles});
                    layout.circles.show(view);
                });
                this.repo.getAllUsers().done(function (users) {
                    var view2 = new CirculosApp.UserListView({collection: users});
                    layout.users.show(view2);
                });
            }
        });


        CirculosApp.addInitializer(function () {
            CirculosApp.controller = new CirculosApp.Controller({repo: CirculosApp.repository, region: RSC.content});
        });

        CirculosApp.addFinalizer(function () {
            CirculosApp.controller.close();
        });

    }
});