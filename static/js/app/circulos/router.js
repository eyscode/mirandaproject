RSC.module("CirculosApp", {
    startWithParent: false,
    define: function (CirculosApp, App, Backbone, Marionette, $, _) {

        // Router
        // -----------

        var Router = Marionette.AppRouter.extend({
            routes: {
                "circulos": "showCircles"
            },
            before: function () {
                App.startSubApp("CirculosApp");
            },
            showCircles: function () {
                CirculosApp.controller.show();
            }
        });

        // Initializer
        // -----------

        App.addInitializer(function () {
            var router = new Router();
        });

    }
});