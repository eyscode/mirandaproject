RSC.module("CirculosApp", function (CirculosApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Nav App Views
    // --------------

    CirculosApp.Layout = Marionette.Layout.extend({
        template: "#circles-layout-template",
        className: "circles-layout",
        regions: {
            circles: ".circles-region",
            users: ".users-region"
        },
        events: {
            "keyup .search-user": "searchUsers"
        },
        searchUsers: function () {
            App.vent.trigger("app:circulos:keypress", this.$(".search-user").val());
        }
    });
    CirculosApp.CircleView = Marionette.ItemView.extend({
        template: "#circle-template",
        className: "circle-item",
        modelEvents: {
            "change": "render"
        }
    });
    CirculosApp.UserView = Marionette.ItemView.extend({
        template: "#user-template",
        className: "user-item",
        templateHelpers: {
            circles: function () {
                return CirculosApp.repository.circles;
            }
        },
        events: {
            "change select": "addTo",
            "mouseenter button.multiselect": "clickButton"
        },
        ui: {
            select: "select"
        },
        onRender: function () {
            var that = this;
            this.ui.select.multiselect({
                nonSelectedText: "Agregar",
                numberDisplayed: 1,
                nSelectedText: "Circulos",
                buttonWidth: "110px",
                buttonText: function (options, select) {
                    if (options.length == 0) {
                        return this.nonSelectedText;
                    }
                    else {
                        if (options.length > this.numberDisplayed) {
                            return '<span class="glyphicon glyphicon-record circle-mini-mini"></span>' + options.length + ' ' + this.nSelectedText;
                        }
                        else {
                            var selected = '';
                            options.each(function () {
                                var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();

                                selected += label + ', ';
                            });
                            return '<span class="glyphicon glyphicon-record circle-mini-mini"></span>' + selected.substr(0, selected.length - 2);
                        }
                    }
                },
                onChange: function (element, checked) {
                    if (checked) {
                        that.$("button.multiselect").addClass("with-color");
                        $.ajax({
                            type: "post",
                            url: "/api/add-user-to-circle/",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify({user: that.model.get("id"), circle: element.val()})
                        }).done(function () {
                                var circle = App.CirculosApp.repository.circles.get(element.val());
                                circle.set("total", circle.get("total") + 1);
                            });
                    } else {
                        if (that.$("option:checked").length == 0) {
                            that.$("button.multiselect").removeClass("with-color");
                        }
                        $.ajax({
                            type: "post",
                            url: "/api/add-user-to-circle/",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify({user: that.model.get("id"), circle: element.val()})
                        }).done(function () {
                                var circle = App.CirculosApp.repository.circles.get(element.val());
                                circle.set("total", circle.get("total") - 1);
                            });
                    }
                }
            });
        }
        /*,
         addTo: function () {
         var circle_id = this.$("option:selected").val();
         CirculosApp.repository.circles.get(circle_id);
         $.$ajax({
         type: "post",
         url: "/api/circles/" + circle_id + "/add-user/",
         data: JSON.stringify({id: this.model.get("id")})
         });
         }*/
    });
    CirculosApp.CircleListView = Marionette.CollectionView.extend({
        itemView: CirculosApp.CircleView
    });
    CirculosApp.UserListView = Marionette.CollectionView.extend({
        itemView: CirculosApp.UserView,
        className: "user-list",
        initialize: function () {
            App.vent.on("app:circulos:keypress", function (text) {
                this.collection.url = "/api/users/" + "?q=" + text;
                this.collection.fetch();
            }, this);
        },
        onClose: function () {
            App.vent.off("app:circulos:keypress");
        }
    });
});