RSC.module("CirculosApp", function (CirculosApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // CirculosApp Models
    // --------------

    CirculosApp.CircleModel = Backbone.Model.extend();

    CirculosApp.CircleCollection = Backbone.Collection.extend({
        url: "/api/me/circles/"
    });

    CirculosApp.UserModel = Backbone.Model.extend();
    CirculosApp.UserCollection = Backbone.Collection.extend({
        url: "/api/users/",
        parse: function (data) {
            return data.results;
        }
    });

    var InfoRespository = Marionette.Controller.extend({
        initialize: function () {
            this.circles = new CirculosApp.CircleCollection();
            this.ajaxCall = this.circles.fetch();
        },
        getCircles: function () {
            var defferred = $.Deferred();
            var that = this;
            this.ajaxCall.done(function () {
                defferred.resolve(that.circles);
            });
            return defferred;
        },
        searchUsers: function (text) {
            var defferred = $.Deferred();
            var users = new CirculosApp.UserCollection();
            users.fetch().done(function () {
                defferred.resolve(users);
            });
            return defferred;
        },
        getAllUsers: function () {
            var defferred = $.Deferred();
            var users = new CirculosApp.UserCollection();
            users.fetch().done(function () {
                defferred.resolve(users);
            });
            return defferred;
        }
    });

    App.addInitializer(function () {
        CirculosApp.repository = new InfoRespository();
    });

});