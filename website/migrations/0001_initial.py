# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Profile'
        db.create_table(u'website_profile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('bio', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('sex', self.gf('django.db.models.fields.BooleanField')()),
            ('job_title', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('department', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('birthday', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('significant_other', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('expertise', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('interests', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('work_phone', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('mobile_phone', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('im', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('twitter_username', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('skype_name', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('facebook_profile', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('linkedin_profile', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('other_websites', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Profile'])

        # Adding model 'Circle'
        db.create_table(u'website_circle', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='circles', to=orm['auth.User'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'website', ['Circle'])

        # Adding M2M table for field people on 'Circle'
        m2m_table_name = db.shorten_name(u'website_circle_people')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('circle', models.ForeignKey(orm[u'website.circle'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['circle_id', 'user_id'])

        # Adding model 'WorkGroup'
        db.create_table(u'website_workgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='workgroups_created', to=orm['auth.User'])),
            ('public', self.gf('django.db.models.fields.BooleanField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'website', ['WorkGroup'])

        # Adding M2M table for field people on 'WorkGroup'
        m2m_table_name = db.shorten_name(u'website_workgroup_people')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('workgroup', models.ForeignKey(orm[u'website.workgroup'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['workgroup_id', 'user_id'])

        # Adding M2M table for field admins on 'WorkGroup'
        m2m_table_name = db.shorten_name(u'website_workgroup_admins')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('workgroup', models.ForeignKey(orm[u'website.workgroup'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['workgroup_id', 'user_id'])

        # Adding model 'Work'
        db.create_table(u'website_work', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('employer', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('start_year', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('end_year', self.gf('django.db.models.fields.CharField')(max_length=4)),
        ))
        db.send_create_signal(u'website', ['Work'])

        # Adding model 'Education'
        db.create_table(u'website_education', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('school', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('degree', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('start_year', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('end_year', self.gf('django.db.models.fields.CharField')(max_length=4)),
        ))
        db.send_create_signal(u'website', ['Education'])

        # Adding model 'Topic'
        db.create_table(u'website_topic', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20, db_index=True)),
        ))
        db.send_create_signal(u'website', ['Topic'])

        # Adding model 'Like'
        db.create_table(u'website_like', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='my_likes', to=orm['auth.User'])),
            ('date_like', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('publication', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='likes', null=True, to=orm['website.Publication'])),
            ('comment', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='likes', null=True, to=orm['website.Comment'])),
        ))
        db.send_create_signal(u'website', ['Like'])

        # Adding model 'Publication'
        db.create_table(u'website_publication', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='pubs_created', to=orm['auth.User'])),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('workgroup', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.WorkGroup'], null=True, blank=True)),
            ('date_pub', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Publication'])

        # Adding M2M table for field people_notified on 'Publication'
        m2m_table_name = db.shorten_name(u'website_publication_people_notified')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('publication', models.ForeignKey(orm[u'website.publication'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['publication_id', 'user_id'])

        # Adding M2M table for field topics on 'Publication'
        m2m_table_name = db.shorten_name(u'website_publication_topics')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('publication', models.ForeignKey(orm[u'website.publication'], null=False)),
            ('topic', models.ForeignKey(orm[u'website.topic'], null=False))
        ))
        db.create_unique(m2m_table_name, ['publication_id', 'topic_id'])

        # Adding M2M table for field circles on 'Publication'
        m2m_table_name = db.shorten_name(u'website_publication_circles')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('publication', models.ForeignKey(orm[u'website.publication'], null=False)),
            ('circle', models.ForeignKey(orm[u'website.circle'], null=False))
        ))
        db.create_unique(m2m_table_name, ['publication_id', 'circle_id'])

        # Adding model 'Comment'
        db.create_table(u'website_comment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('pub', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Publication'])),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('date_comment', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Comment'])

        # Adding model 'Message'
        db.create_table(u'website_message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('from_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='messages_received', to=orm['auth.User'])),
            ('to_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='messages_sent', to=orm['auth.User'])),
            ('unread', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('date_sent', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('date_read', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Message'])


    def backwards(self, orm):
        # Deleting model 'Profile'
        db.delete_table(u'website_profile')

        # Deleting model 'Circle'
        db.delete_table(u'website_circle')

        # Removing M2M table for field people on 'Circle'
        db.delete_table(db.shorten_name(u'website_circle_people'))

        # Deleting model 'WorkGroup'
        db.delete_table(u'website_workgroup')

        # Removing M2M table for field people on 'WorkGroup'
        db.delete_table(db.shorten_name(u'website_workgroup_people'))

        # Removing M2M table for field admins on 'WorkGroup'
        db.delete_table(db.shorten_name(u'website_workgroup_admins'))

        # Deleting model 'Work'
        db.delete_table(u'website_work')

        # Deleting model 'Education'
        db.delete_table(u'website_education')

        # Deleting model 'Topic'
        db.delete_table(u'website_topic')

        # Deleting model 'Like'
        db.delete_table(u'website_like')

        # Deleting model 'Publication'
        db.delete_table(u'website_publication')

        # Removing M2M table for field people_notified on 'Publication'
        db.delete_table(db.shorten_name(u'website_publication_people_notified'))

        # Removing M2M table for field topics on 'Publication'
        db.delete_table(db.shorten_name(u'website_publication_topics'))

        # Removing M2M table for field circles on 'Publication'
        db.delete_table(db.shorten_name(u'website_publication_circles'))

        # Deleting model 'Comment'
        db.delete_table(u'website_comment')

        # Deleting model 'Message'
        db.delete_table(u'website_message')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'website.circle': {
            'Meta': {'object_name': 'Circle'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'people': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'circled'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'circles'", 'to': u"orm['auth.User']"})
        },
        u'website.comment': {
            'Meta': {'object_name': 'Comment'},
            'date_comment': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Publication']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'website.education': {
            'Meta': {'object_name': 'Education'},
            'degree': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end_year': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'school': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'start_year': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'website.like': {
            'Meta': {'object_name': 'Like'},
            'comment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'likes'", 'null': 'True', 'to': u"orm['website.Comment']"}),
            'date_like': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'likes'", 'null': 'True', 'to': u"orm['website.Publication']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'my_likes'", 'to': u"orm['auth.User']"})
        },
        u'website.message': {
            'Meta': {'object_name': 'Message'},
            'date_read': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_sent': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'from_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'messages_received'", 'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'to_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'messages_sent'", 'to': u"orm['auth.User']"}),
            'unread': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'website.profile': {
            'Meta': {'object_name': 'Profile'},
            'bio': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'expertise': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'facebook_profile': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'im': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'interests': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'job_title': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'linkedin_profile': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mobile_phone': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'other_websites': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'sex': ('django.db.models.fields.BooleanField', [], {}),
            'significant_other': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'skype_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'twitter_username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'work_phone': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'})
        },
        u'website.publication': {
            'Meta': {'object_name': 'Publication'},
            'circles': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Circle']", 'null': 'True', 'blank': 'True'}),
            'date_pub': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'people_notified': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'pubs_mentioned'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'topics': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Topic']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pubs_created'", 'to': u"orm['auth.User']"}),
            'workgroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.WorkGroup']", 'null': 'True', 'blank': 'True'})
        },
        u'website.topic': {
            'Meta': {'object_name': 'Topic'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'})
        },
        u'website.work': {
            'Meta': {'object_name': 'Work'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'employer': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'end_year': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_year': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'website.workgroup': {
            'Meta': {'object_name': 'WorkGroup'},
            'admins': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'workgroups_admin'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'people': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'workgroups_joined'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'public': ('django.db.models.fields.BooleanField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'workgroups_created'", 'to': u"orm['auth.User']"})
        }
    }

    complete_apps = ['website']