from django.contrib.auth.models import User
from django.db import models
from django.db.models.fields.files import ImageFieldFile, FileField


class Profile(models.Model):
    user = models.OneToOneField(User)
    photo = models.ImageField(upload_to="users/photos/", null=True, blank=True)
    # Info
    bio = models.TextField(null=True, blank=True)
    sex = models.BooleanField()
    job_title = models.CharField(max_length=50, null=True, blank=True)
    department = models.CharField(max_length=40, null=True, blank=True)
    location = models.CharField(max_length=100, null=True, blank=True)
    birthday = models.DateField(null=True, blank=True)
    significant_other = models.CharField(max_length=100, null=True, blank=True)
    expertise = models.TextField(null=True, blank=True)
    interests = models.TextField(null=True, blank=True)
    # Contact
    work_phone = models.CharField(max_length=10, null=True, blank=True)
    mobile_phone = models.CharField(max_length=10, null=True, blank=True)
    im = models.CharField(max_length=20, null=True, blank=True)
    twitter_username = models.CharField(max_length=30, null=True, blank=True)
    skype_name = models.CharField(max_length=30, null=True, blank=True)
    facebook_profile = models.CharField(max_length=50, null=True, blank=True)
    linkedin_profile = models.CharField(max_length=50, null=True, blank=True)
    other_websites = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def unique_name(self):
        return self.user.username[:self.user.username.find("@")]

    def get_thumb(self):
        from sorl.thumbnail import get_thumbnail

        if self.photo:
            im = get_thumbnail(self.photo, '200x200', crop='top') if self.photo else None
            return ImageFieldFile(instance=im, field=FileField(), name=im.name if im else None).url
        return None

    def get_photo(self):
        return self.photo.url if self.photo else None

    def get_sex(self):
        return "male" if self.sex else "female"

    def __unicode__(self):
        return self.user.username


class Circle(models.Model):
    user = models.ForeignKey(User, related_name="circles")
    name = models.CharField(max_length=20)
    people = models.ManyToManyField(User, null=True, blank=True, related_name="circled")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def total(self):
        return self.people.count()

    def __unicode__(self):
        return self.name


class WorkGroup(models.Model):
    user = models.ForeignKey(User, related_name="workgroups_created")
    public = models.BooleanField()
    name = models.CharField(max_length=200)
    description = models.TextField()
    people = models.ManyToManyField(User, null=True, blank=True, related_name="workgroups_joined")
    admins = models.ManyToManyField(User, null=True, blank=True, related_name="workgroups_admin")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Work(models.Model):
    user = models.ForeignKey(User, related_name="work_list")
    employer = models.CharField(max_length=100)
    title = models.CharField(max_length=50)
    description = models.TextField()
    start_year = models.CharField(max_length=4)
    end_year = models.CharField(max_length=4, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']

    def __unicode__(self):
        return self.title + " - " + self.employer


class Education(models.Model):
    user = models.ForeignKey(User, related_name="education_list")
    school = models.CharField(max_length=100)
    degree = models.CharField(max_length=100)
    description = models.TextField()
    start_year = models.CharField(max_length=4)
    end_year = models.CharField(max_length=4, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']

    def __unicode__(self):
        return self.degree + " - " + self.school


class Topic(models.Model):
    name = models.CharField(max_length=20, db_index=True)

    def __unicode__(self):
        return self.name


class Like(models.Model):
    user = models.ForeignKey(User, related_name="my_likes")
    date_like = models.DateTimeField(auto_now_add=True)
    publication = models.ForeignKey("Publication", null=True, blank=True, related_name="likes")
    comment = models.ForeignKey("Comment", null=True, blank=True, related_name="likes")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @staticmethod
    def user_likes_comment(user, obj_id):
        if Like.objects.values("id").filter(user=user, comment__id=obj_id):
            return True
        return False

    @staticmethod
    def user_likes_pub(user, obj_id):
        if Like.objects.values("id").filter(user=user, publication__id=obj_id):
            return True
        return False


class Publication(models.Model):
    user = models.ForeignKey(User, related_name="pubs_created")
    people_notified = models.ManyToManyField(User, null=True, blank=True, related_name="pubs_mentioned")
    text = models.TextField()
    topics = models.ManyToManyField(Topic, null=True, blank=True)
    file = models.FileField(upload_to="users/files_upload/", null=True, blank=True)
    circles = models.ManyToManyField(Circle, null=True, blank=True)
    workgroup = models.ForeignKey(WorkGroup, null=True, blank=True)
    date_pub = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def public(self):
        return True if self.circles.count() == 0 else False

    @property
    def total_likes(self):
        return self.likes.count()

    def __unicode__(self):
        return self.text[:10]

    def save(self, *args, **kwargs):
        import re

        self.text = re.sub("\\n\s*", "\\n", self.text)
        return super(Publication, self).save()

    @property
    def text_escaped(self):
        from django.utils.html import escape
        import re

        escaped = escape(self.text).replace("\n", "<br>")
        escaped = re.sub(r"(((http://|https://|ftp://)([^\(\)\s]+\.[^\(\)\s]+))|www\.([^\(\)\s]+\.[^\(\)\s]+))",
                         r"<a target=_blanck href='\1'>\1</a>", escaped)
        return escaped


class Comment(models.Model):
    user = models.ForeignKey(User)
    pub = models.ForeignKey(Publication)
    text = models.TextField()
    date_comment = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def total_likes(self):
        return self.likes.count()

    def __unicode__(self):
        return u"{}: {}".format(self.user.username, self.text[:10])

    def save(self, *args, **kwargs):
        import re

        self.text = re.sub("\\n\s*", "\\n", self.text)
        return super(Comment, self).save()

    @property
    def text_escaped(self):
        from django.utils.html import escape
        import re

        escaped = escape(self.text).replace("\n", "<br>")
        escaped = re.sub(r"(((http://|https://|ftp://)([^\(\)\s]+\.[^\(\)\s]+))|www\.([^\(\)\s]+\.[^\(\)\s]+))",
                         r"<a target=_blanck href='\1'>\1</a>", escaped)
        return escaped


class Message(models.Model):
    from_user = models.ForeignKey(User, related_name="messages_received")
    to_user = models.ForeignKey(User, related_name="messages_sent")
    unread = models.BooleanField(default=True)
    text = models.TextField()
    date_sent = models.DateTimeField(auto_now_add=True)
    date_read = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = "date_sent",