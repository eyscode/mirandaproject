from rest_framework import serializers
from website.fields import PubLikedByUserField, CommentLikedByUserField
from website.models import Publication, User, Profile, Comment, Work, Education, Message, Circle


class ProfileSerializer(serializers.ModelSerializer):
    sex = serializers.CharField(source="get_sex")
    photo = serializers.CharField(source="get_photo")

    class Meta:
        model = Profile
        exclude = "id", "user"


class WorkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Work
        exclude = "user",


class EducationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Education
        exclude = "user",


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = User
        fields = "id", "first_name", "last_name", "profile", "email"


class UserMiniSerializer(serializers.ModelSerializer):
    fullname = serializers.CharField(source="get_full_name")
    photo = serializers.CharField(source="profile.get_thumb")

    class Meta:
        model = User
        fields = "id", "photo", "fullname", "email"


class UserOnlineSerializer(serializers.ModelSerializer):
    fullname = serializers.CharField(source="get_full_name")
    photo = serializers.CharField(source="profile.get_thumb")
    online = serializers.BooleanField(source="online")
    count_offline = serializers.IntegerField(source="count_offline")

    class Meta:
        model = User
        fields = "id", "photo", "fullname", "email", "online"


class PublicationSerializer(serializers.ModelSerializer):
    user = UserMiniSerializer()
    workgroup_name = serializers.CharField(source="workgroup.name")
    workgroup_id = serializers.CharField(source="workgroup.id")
    likes = serializers.IntegerField(source="total_likes")
    is_public = serializers.BooleanField(source="public")
    melike = PubLikedByUserField(source="id", read_only=True)
    text = serializers.CharField(source="text_escaped")

    class Meta:
        model = Publication
        fields = "id", "user", "text", "topics", "file", "date_pub", "workgroup_id", "workgroup_name", "likes", "melike", "is_public"


class PublicationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = "id", "text", "user"


class CommentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment


class CommentSerializer(serializers.ModelSerializer):
    user = UserMiniSerializer()
    likes = serializers.IntegerField(source="total_likes", read_only=True)
    melike = CommentLikedByUserField(source="id", read_only=True)
    text = serializers.CharField(source="text_escaped")

    class Meta:
        model = Comment


class MessageSerializer(serializers.ModelSerializer):
    from_photo = serializers.CharField(source="from_user.profile.get_thumb", read_only=True)
    to_photo = serializers.CharField(source="to_user.profile.get_thumb", read_only=True)

    class Meta:
        model = Message
        exclude = "unread", "date_read"


class CircleSerializer(serializers.ModelSerializer):
    total = serializers.IntegerField(source="total", read_only=True)

    class Meta:
        model = Circle
        fields = "id", "user", "name", "total"