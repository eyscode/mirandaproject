from django.contrib import admin
from website.models import Profile, Publication, Circle, WorkGroup, Like, Work, Education, Topic, Comment, Message

admin.site.register(Profile)
admin.site.register(Publication)
admin.site.register(Circle)
admin.site.register(WorkGroup)
admin.site.register(Like)
admin.site.register(Work)
admin.site.register(Education)
admin.site.register(Topic)
admin.site.register(Comment)
admin.site.register(Message)