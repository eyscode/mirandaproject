from django.core.cache import get_cache
from django.db.models.query_utils import Q
from rest_framework import generics, status, mixins
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from website.models import Publication, User, Circle, Like, Comment, Work, Education, Message
from website.serializers import PublicationSerializer, UserSerializer, PublicationCreateSerializer, CommentSerializer, CommentCreateSerializer, WorkSerializer, EducationSerializer, UserMiniSerializer, UserOnlineSerializer, MessageSerializer, CircleSerializer
import pika


class PostListCreate(generics.ListCreateAPIView):
    serializer_class = PublicationSerializer
    model = Publication
    paginate_by = 10

    def get_queryset(self):
        return self.model.objects.filter(
            Q(user=self.request.user) |
            Q(people_notified__in=(self.request.user.id,)) |
            (
                (
                    Q(circles__in=Circle.objects.filter(people__in=(self.request.user.id,))) |
                    # el compartio el post con un circulo donde me tiene
                    Q(circles=None)  # el post es publico
                ) &
                Q(user__in=User.objects.filter(circled__in=self.request.user.circles.all()))
                # yo siga al que publico el post, se sigue siempre y cuando el este en mis circulos
            )
        ).order_by("-date_pub")

    def create(self, request, *args, **kwargs):
        new_data = request.DATA
        new_data["user"] = request.user.id
        serializer = PublicationCreateSerializer(data=new_data, files=request.FILES)

        circles_ids = request.DATA.get("values")
        if 0 in circles_ids:
            circles_ids.remove(0)
        circles = Circle.objects.filter(id__in=circles_ids)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)
            self.object.circles.add(*list(circles))
            headers = self.get_success_headers(serializer.data)
            return Response(PublicationSerializer(self.object, context=self.get_serializer_context()).data,
                            status=status.HTTP_201_CREATED,
                            headers=headers)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MeGet(generics.RetrieveUpdateAPIView):
    model = User
    serializer_class = UserSerializer

    def get_object(self, queryset=None):
        return self.request.user


class GetUser(generics.RetrieveAPIView):
    model = User
    serializer_class = UserSerializer

    def get_object(self, queryset=None):
        uuid = self.kwargs.get("uuid")
        return self.model.objects.get(username__startswith=uuid + "@")


class MeWork(generics.ListCreateAPIView):
    model = Work
    serializer_class = WorkSerializer

    def get_queryset(self):
        return self.request.user.work_list.all()

    def pre_save(self, obj):
        obj.user = self.request.user


class UserWorks(generics.ListAPIView):
    model = Work
    serializer_class = WorkSerializer

    def get_queryset(self):
        uuid = self.kwargs.get("uuid")
        return self.model.objects.filter(user__username__startswith=uuid + "@")


class MeWorkIndividual(generics.UpdateAPIView, mixins.DestroyModelMixin):
    model = Work
    serializer_class = WorkSerializer

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class MeEducation(generics.ListCreateAPIView):
    model = Education
    serializer_class = EducationSerializer

    def get_queryset(self):
        return self.request.user.education_list.all()

    def pre_save(self, obj):
        obj.user = self.request.user


class UserEducations(generics.ListAPIView):
    model = Education
    serializer_class = EducationSerializer

    def get_queryset(self):
        uuid = self.kwargs.get("uuid")
        return self.model.objects.filter(user__username__startswith=uuid + "@")


class MeEducationIndividual(generics.UpdateAPIView, mixins.DestroyModelMixin):
    model = Education
    serializer_class = EducationSerializer

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ToggleLikePost(generics.GenericAPIView):
    model = Like

    def post(self, request, *args, **kwargs):
        obj_id = kwargs.get("pk")
        if obj_id:
            pub = Publication.objects.filter(id=obj_id)
            if pub:
                like = Like.objects.filter(user=request.user, publication=pub)
                if like:
                    like[0].delete()
                    return Response({"like": False}, status=status.HTTP_200_OK)
                else:
                    Like.objects.create(user=request.user, publication=pub[0])
                    return Response({"like": True}, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class AddUserToCircle(generics.GenericAPIView):
    def post(self, request, *args, **kwargs):
        user = request.DATA.get("user")
        circle = request.DATA.get("circle")
        user = User.objects.get(id=user)
        circle = Circle.objects.get(id=circle)
        if circle.people.filter(id=user.id):
            added = False
            circle.people.remove(user)
        else:
            added = True
            circle.people.add(user)
        return Response({"added": added}, status=status.HTTP_200_OK)


class ToggleLikeComment(generics.GenericAPIView):
    model = Like

    def post(self, request, *args, **kwargs):
        obj_id = kwargs.get("pk")
        if obj_id:
            comment = Comment.objects.filter(id=obj_id)
            if comment:
                like = Like.objects.filter(user=request.user, comment=comment)
                if like:
                    like[0].delete()
                    return Response({"like": False}, status=status.HTTP_200_OK)
                else:
                    Like.objects.create(user=request.user, comment=comment[0])
                    return Response({"like": True}, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class CommentListCreate(generics.ListCreateAPIView):
    model = Comment
    serializer_class = CommentSerializer
    paginate_by = 5

    def get_queryset(self):
        return self.model.objects.filter(pub=self.kwargs.get("comment_id"))

    def create(self, request, *args, **kwargs):
        new_data = request.DATA
        new_data["user"] = request.user.id
        new_data["pub"] = self.kwargs.get("comment_id")
        serializer = CommentCreateSerializer(data=new_data, files=request.FILES)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)
            headers = self.get_success_headers(serializer.data)
            return Response(self.serializer_class(self.object, context=self.get_serializer_context()).data,
                            status=status.HTTP_201_CREATED,
                            headers=headers)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListContacts(generics.ListAPIView):
    model = User
    serializer_class = UserOnlineSerializer

    def get_queryset(self):
        contactos = User.objects.filter(Q(
            id__in=User.objects.filter(circled__in=self.request.user.circles.all()).values("id")) & Q(
            circles__in=Circle.objects.filter(people__in=(self.request.user.id,))))
        cache = get_cache("default")
        online_users = cache.get("online_users", set())
        if not self.request.user in online_users:
            self.request.user.count_offline = 0
            self.request.user.online = True
            online_users.add(self.request.user)
            cache.set("online_users", online_users)
        for c in contactos:
            if c in online_users:
                c.online = True
            else:
                c.online = False
        return contactos


class ListCircles(generics.ListAPIView):
    model = Circle
    serializer_class = CircleSerializer

    def get_queryset(self):
        return self.request.user.circles.all()


class SendMessage(generics.GenericAPIView):
    def post(self, request, *args, **kwargs):
        text = request.DATA.get("text")
        receptor_id = request.DATA.get("user")
        receptor = User.objects.filter(id=receptor_id)
        if receptor_id != request.user.id and receptor:
            m = Message.objects.create(from_user=request.user, to_user=receptor[0], text=text)
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                host='localhost'))
            channel = connection.channel()
            channel.queue_declare(queue='miranda')
            data = MessageSerializer(m).data
            channel.basic_publish(exchange='',
                                  routing_key='miranda',
                                  properties=pika.BasicProperties(content_type="application/json"),
                                  body=JSONRenderer().render(data))
            connection.close()
            return Response(MessageSerializer(m).data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class LastMessages(generics.ListAPIView):
    model = Message
    serializer_class = MessageSerializer

    def get_queryset(self):
        contact = self.kwargs.get("pk")
        return Message.objects.filter(
            Q(from_user=self.request.user, to_user__id=contact) | Q(from_user__id=contact,
                                                                    to_user=self.request.user))


class ListUsers(generics.ListAPIView):
    model = User
    serializer_class = UserMiniSerializer
    paginate_by = 15

    def get_queryset(self):
        q = self.request.GET.get("q")
        if q:
            return self.model.objects.filter(Q(first_name__icontains=q) | Q(last_name__icontains=q))
        return super(ListUsers, self).get_queryset()