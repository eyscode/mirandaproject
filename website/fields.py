from rest_framework import fields
from website.models import Like


class PubLikedByUserField(fields.WritableField):
    def to_native(self, obj):
        request = self.context.get('request', None)
        return Like.user_likes_pub(request.user, obj)


class CommentLikedByUserField(fields.WritableField):
    def to_native(self, obj):
        request = self.context.get('request', None)
        return Like.user_likes_comment(request.user, obj)
